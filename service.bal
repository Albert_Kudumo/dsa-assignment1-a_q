
import ballerina/grpc;

listener grpc:Listener ep = new (9091);

isolated map<metadata> funtionMetadata = {};


@grpc:ServiceDescriptor {
    descriptor: ROOT_DESCRIPTOR, 
    descMap: getDescriptorMap()
}
service "FunctionRepository" on ep {

    remote function add_new_fn(metadata functionDetails) returns string|error {

        string versionId = functionDetails.Version;

        addFunctionDetails(versionId, functionDetails);

        string payload = "Status : function created; function version : " + versionId;

        return payload;
    }

    remote function add_fns(stream<metadata> functionDetails) returns string|error {
        string[] createdVersions = [];

        foreach var item in functionDetails {
            string versionId = item.Version;
            createdVersions.push(versionId);
            addFunctionDetails(versionId, item);
        }

        string payload = "Status : functions created: " + createdVersions.toJsonString();

        return payload;
    }

    remote function delete_fn(string versionId) returns string|error {
        string payload;
        json result = getJson(versionId);

        deleteFunctionDetails(versionId);

        payload = "function version : '" + versionId + "' removed.";

        return payload;
    }

    remote function show_fn(string versionId) returns string|error {
        json Result = {};
        Result = getFunctionDetails(versionId);

        return Result.toString();
    }


    remote function show_all_fns(string versionId) returns stream<string, error?>|error {
        metadata [] Result = getAllFunctionDetails();

        string[] versions =[];

         int i = 0;
        foreach var item in Result {
            versions[i] =item.Version;
            i += 1;
        }

        return  versions.toStream();
    }

    remote function show_all_with_criteria(FunctionRepositoryStringCaller caller, stream<metadata, error?> clientStream) returns error? {
         metadata [] Result =getAllFunctionDetails();
        
        Result.forEach(function(metadata funVersion) {
            checkpanic caller->sendString(string `Version: ${funVersion.Version}`);
        });

        check caller->complete();
    }
}

isolated function addFunctionDetails(string versionId, metadata functionDetails) {
    lock {
        funtionMetadata[versionId] = functionDetails.clone();
    }
}

isolated function deleteFunctionDetails(string versionId) {
    lock {
        _ = funtionMetadata.remove(versionId);
    }
}

isolated function getJson(string versionId) returns json {
    lock {
        return funtionMetadata.hasKey(versionId);
    }
}

isolated function getFunctionDetails(string versionId) returns json {
    lock {
        return funtionMetadata[versionId].clone();
    }
}

isolated function getAllFunctionDetails() returns metadata[] {
    lock {
        return funtionMetadata.clone().toArray();
    }
}

