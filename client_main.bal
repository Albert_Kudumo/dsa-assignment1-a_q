import ballerina/io;
import ballerina/http;


public function main() returns @tainted error? {
    http:Client httpClient = check new ("http://localhost:4000/learner");
    string learnerId = "";

     //POST METHOD
    
    json resp = check httpClient->post("/create", Profile);
    result success = check resp.cloneWithType(result);
    learnerId = success.learnerId;
    io:println(resp.toJsonString());

     //GET METHOD
    json firstResp = check httpClient->get("/fromLearnerById?learnerId=" + learnerId);
    io:println(firstResp.toJsonString());
    json Materials = check httpClient->get("/learningMaterials");

     io:println("\nLearning Materials");
    io:println(Materials.toJsonString());
    

     //PUT METHOD
     Learner UpdateJson = check Profile.cloneWithType(Learner);
    UpdateJson.firstname = "Queen";
    UpdateJson.lastname = "Imbili";
    json UpdateResp = check httpClient->put("/update?learnerId=" + learnerId, UpdateJson.toJson());
    io:println(UpdateResp.toJsonString());
    

    json SecResponse = check httpClient->get("/fromLearnerById?learnerId=" + learnerId);
    io:println(SecResponse.toJsonString());

    
final json Profile = 
{
    "username": "Nsala", 
    "lastname": "Mbeha", 
    "firstname": "Riaan", 
    "preferred_formats": ["audio", "video", "text"], 
        "past_subjects": [
            {"course": "Algorithms", 
                "score": "B+"
            }, 
            {
                "course": "Programming I", 
                "score": "A+"
            }
        ]
};
type result record {
    string status;
    string learnerId;
};

public type Subjects record {|
    string course;
    string score;
|};

public type Learner record {|
    string username;
    string lastname;
    string firstname;
    string[] preferred_formats;
    Subjects[] past_subjects;
|};
